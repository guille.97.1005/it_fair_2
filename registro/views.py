from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Persona
#from .models import Usuario
#from .models import Rescatado
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout, login as login_user
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm


# Create your views here.

def login(request):
    return render(request,'login.html',{})

def index(request):
    return render(request,'index.html',{})


def registro(request):
    return render(request,'formulario.html',{})

def crear(request):
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    edad = request.POST.get('edad',0)
    estado= request.POST.get('estado','INGRESO')
    fechaNac= request.POST.get('Fecha','')
    ecivil= request.POST.get('ecivil','')
    paisOrig= request.POST.get('paisOrig','')
    nacionalidad=  request.POST.get('nacionalidad','')
    tipoDoc= request.POST.get('tipoDoc','')
    numDoc= request.POST.get('numDoc',0)
    persona = Persona(nombre=nombre,apellido=apellido,edad=edad, estado=estado,fechaNac=fechaNac,ecivil=ecivil,paisOrig=paisOrig,nacionalidad=nacionalidad,tipoDoc=tipoDoc,numDoc=numDoc)
    persona.save()
    return HttpResponse("nombre :"+nombre+" apellido :"+apellido+" Edad :"+edad)

def buscar(request,id):
    persona = Persona.objects.get(pk=id)
    return HttpResponse("nombre :"+persona.nombre+" apellido :"+persona.apellido)

def editar(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request,'editar.html',{'persona':persona})

def editado(request,id):
    persona = Persona.objects.get(pk=id)
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    edad = request.POST.get('edad',0)
    estado= request.POST.get('estado','INGRESO')
    fechaNac= request.POST.get('Fecha','')
    ecivil= request.POST.get('ecivil','')
    paisOrig= request.POST.get('paisOrig','')
    nacionalidad=  request.POST.get('nacionalidad','')
    tipoDoc= request.POST.get('tipoDoc','')
    numDoc= request.POST.get('numDoc',0)

    persona.nombre = nombre
    persona.apellido = apellido
    persona.edad = edad
    persona.estado = estado
    persona.fechaNac= fechaNac 
    persona.ecivil= ecivil
    persona.paisOrig= paisOrig
    persona.nacionalidad= nacionalidad
    persona.tipoDoc= tipoDoc
    persona.numDoc= numDoc
    persona.save()
    return HttpResponse("nombre :"+persona.nombre+" apellido :"+persona.apellido)

def eliminar(request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return HttpResponse("Persona Eliminada")

def main(request):
    return render(request, 'index.html')

def formulario2(request):
    return render(request, 'formulario2.html',{})

def cerrar_sesion(request):
    logout(request)
    return redirect("login")

def logearse(request):
    nombre = request.POST.get("usuario",False)
    contrasenia = request.POST.get("contrasenia",False)
    user = authenticate(request, username=nombre, password=contrasenia)
    print(nombre,contrasenia)
    print(user)
    if user is not None:
        login_user(request, user)
        return redirect("index")
    else:
        messages.error(request,'El usuario o la contraseña no es válido ')
        return redirect("login")