from django.db import models

# Create your models here.

class Persona(models.Model):
    nombre= models.CharField(max_length=100)
    apellido= models.CharField(max_length=100)
    apellido_mat= models.CharField(max_length=100) #
    edad= models.IntegerField()
    estado= models.CharField(max_length=100) #
    fechaNac= models.DateField(null=True) #     
    ecivil= models.CharField(max_length=20) #
    paisOrig= models.CharField(max_length=20) #
    nacionalidad= models.CharField(max_length=20) #
    tipoDoc= models.CharField(max_length=20) #
    numDoc= models.IntegerField() #
    
    #en pagina de django hay mas info de esto
     #así se hace un to_string en python
    def __str__(self):
        return "nombre "+self.nombre+" apellido "+self.apellido
