from django.urls import path
from . import views
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    #path('',views.index,name="index"),
    path('',views.index,name="index"),
    path('login', views.login, name="login"),
    path('cerrar_sesion', views.cerrar_sesion, name="cerrar_sesion"),
    path('logearse', views.logearse, name="logearse"),
    path('registro/',views.registro,name="registro"),
    path('formulario2',views.formulario2, name="formulario2"),
    path('registro/crear',views.crear, name="crear"),
    path('registro/buscar/<int:id>',views.buscar, name="buscar"),
    path('registro/editar/<int:id>',views.editar, name="editar"),
    path('registro/editado/<int:id>',views.editado, name="editado"),
    path('registro/eliminar/<int:id>',views.eliminar,name="eliminar")
]
